package main

import (
	"context"
	"fmt"
	"ftsystem.com/test-alarm-manager/rpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"io"
	"log"
	"time"
	//"google.golang.org/grpc/status"
	//"google.golang.org/grpc/codes"
)

func main() {

	fmt.Println("Hello I'm a external_agent_test")

	tls := false
	//tls := true
	opts := grpc.WithInsecure()
	if tls {
		certFile := "../ssl/ca.crt" // Certificate Authority Trust certificate
		creds, sslErr := credentials.NewClientTLSFromFile(certFile, "")
		if sslErr != nil {
			log.Fatalf("Error while loading CA trust certificate: %v", sslErr)
			return
		}
		opts = grpc.WithTransportCredentials(creds)
	}

	//cc, err := grpc.Dial("localhost:50051", opts)
	cc, err := grpc.Dial("0.0.0.0:50051", opts)
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer cc.Close()

	c := rpc.NewAlarmsServiceClient(cc)
	// tc := alarmspb.NewTestGreetServiceClient(cc)
	// fmt.Printf("Created external_agent_test: %f", c)
	// doUnary(c)
	// doTestUnary(tc)
	doServerStreaming(c)
	// doClientStreaming(c)
	// doBiDiStreaming(c)
	// doUnaryWithDeadline(c, 5*time.Second) // should complete
	// doUnaryWithDeadline(c, 1*time.Second) // should timeout
}


func doServerStreaming(c rpc.AlarmsServiceClient) {
	fmt.Println("Starting to do a Server Streaming RPC...")

	id := fmt.Sprintf("ID%d", time.Now().Nanosecond())
	req := &rpc.GetAlarmStreamRequest{Param1: id}

	ctx , cancel := context.WithCancel(context.Background())
	defer cancel()

	resStream, err := c.GetAlarmsStream(ctx, req)
	if err != nil {
		log.Fatalf("error while calling GreetManyTimes RPC: %v", err)
	}

	for {
		msg, err := resStream.Recv()
		if err == io.EOF {
			log.Printf("we've reached the end of the stream")
			break
		}
		if err != nil {
			log.Fatalf("error while reading stream: %v", err)
		}
		log.Printf("AlarmData: %v", msg.AlarmData)
	}

}