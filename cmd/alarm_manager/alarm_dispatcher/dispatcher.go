package alarm_dispatcher

import (
	"fmt"
	"ftsystem.com/test-alarm-manager/cmd/alarm_manager/alarm_engine"
)

type AlarmDispatcher struct {
	handlerPoller *alarm_engine.HandlerPoller
}

func newAlarmDispatcher() *AlarmDispatcher {
	return &AlarmDispatcher{
		handlerPoller: alarm_engine.NewHandlerPoller(500),
	}
}

var instance *AlarmDispatcher
func AlarmDispatcherInstance() *AlarmDispatcher {
	if instance == nil {
		instance = newAlarmDispatcher()
	}
	return instance
}

func onHandlerResponse (data interface{}) {
	fmt.Println("Handle response", data.(string))
	//TODO
	// - StoreAlarms
	// - UpdateActionsList
	// - UpdateVariationList
}

func (a *AlarmDispatcher) StartHandlerPoller() {
	a.handlerPoller.Start(onHandlerResponse)
}
