package alarm_engine

import (
	"context"
	"ftsystem.com/test-alarm-manager/cmd/alarm_manager/commons"
	"time"
)

type HandlerPoller struct {
	ctx 	context.Context
	cancel  context.CancelFunc
	tick    int
}


func NewHandlerPoller(tick int) *HandlerPoller {
	p :=  &HandlerPoller{
		tick: tick,
	}
	p.initContext()

	return p
}

func (p *HandlerPoller) initContext() {
	p.ctx, p.cancel = context.WithCancel(context.Background())
}

func (p *HandlerPoller) Start(callbackFn commons.HandleGetAlarmResponseFn) error {
	go func() {
		for {
			select {
			case <-p.ctx.Done():
				return
			default:
				//GetAlarms
				callbackFn("test")
				time.Sleep(time.Duration(p.tick) * time.Millisecond)
			}
		}
	}()

	return nil
}

func (p *HandlerPoller) Stop()  {
	p.cancel()
}
