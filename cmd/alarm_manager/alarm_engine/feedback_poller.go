package alarm_engine

import (
	"context"
	"ftsystem.com/test-alarm-manager/cmd/alarm_manager/commons"
	"time"
)

type FeedbackPoller struct {
	ctx 	context.Context
	cancel  context.CancelFunc
	tick    int
}


func NewFeedbackPoller(tick int) *FeedbackPoller {
	p :=  &FeedbackPoller{
		tick: tick,
	}
	p.initContext()

	return p
}

func (p *FeedbackPoller) initContext() {
	p.ctx, p.cancel = context.WithCancel(context.Background())
}

func (p *FeedbackPoller) Start(callbackFn commons.HandleGetAlarmResponseFn) error {
	go func() {
		for {
			select {
			case <-p.ctx.Done():
				return
			default:
				//ActionDispatcher.GetDeviceActions

				//
				callbackFn("test")
				time.Sleep(time.Duration(p.tick) * time.Millisecond)
			}
		}
	}()

	return nil
}

func (p *FeedbackPoller) Stop()  {
	p.cancel()
}
