package commons

import (
	"ftsystem.com/test-alarm-manager/rpc"
	"sync"
)

type Listener struct {
	Stream     rpc.AlarmsService_GetAlarmsStreamServer
	Data       string
	OutChannel chan int
	Tick       int
}

func NewListener(stream rpc.AlarmsService_GetAlarmsStreamServer, data interface{}, tick int) *Listener {
	return &Listener{
		Stream:     stream,
		Data:       data.(string),
		OutChannel: make(chan int),
		Tick:       tick,
	}
}

type Listeners struct {
	items []*Listener
	mu	 sync.Mutex
}

func (l *Listeners) Add(li *Listener) {
	l.mu.Lock()
	l.items = append(l.items, li)
	l.mu.Unlock()
}

func (l *Listeners) RemoveAt(idx int) {
	l.mu.Lock()
	tmp := append(l.items[:idx], l.items[idx+1:]...)
	l.items = tmp
	l.mu.Unlock()
}

func (l *Listeners) DumpList() []*Listener {
	l.mu.Lock()
	rv := l.items
	l.mu.Unlock()
	return rv
}


