package commons

type Alarm struct {
	key string
	value int
}

type HandleGetAlarmResponseFn func(data interface{})
