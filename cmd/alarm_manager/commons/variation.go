package commons

import (
	"ftsystem.com/golang-ring"
)


type VariationList struct {
	variations ring.Ring
}

func NewVariationList(size int) *VariationList {
	r := ring.Ring{}
	r.SetCapacity(size)
	return &VariationList{
		variations: r,
	}
}



func (v *VariationList) Add(a *Alarm)  {
	v.variations.Enqueue(a)
}

func (v *VariationList) getLastAlarm() (*Alarm)  {
	a := v.variations.Dequeue().(*Alarm)
	return a
}

func (v *VariationList) dumpAlarms() ([]*Alarm) {
	values := v.variations.Values()
	var rv []*Alarm
	for _, v := range values {
		rv = append(rv, v.(*Alarm))
	}
	return rv
}


