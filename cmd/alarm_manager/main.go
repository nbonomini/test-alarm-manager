package main

import (
	"fmt"
	//"ftsystem.com/test-alarm-manager/cmd/alarm_manager/commons"
	"ftsystem.com/test-alarm-manager/rpc/alarm_manager/handler"

	//"ftsystem.com/test-alarm-manager/cmd/alarm_manager"
	"ftsystem.com/test-alarm-manager/rpc"
	"google.golang.org/grpc"
	"log"
	"net"
	//"google.golang.org/grpc/codes"
	//"google.golang.org/grpc/status"
	"google.golang.org/grpc/credentials"
)

func main() {
	fmt.Println("Hello world")

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	opts := []grpc.ServerOption{}
	//tls := true
	tls := false
	if tls {
		certFile := "../ssl/server.crt"
		keyFile := "../ssl/server.pem"
		creds, sslErr := credentials.NewServerTLSFromFile(certFile, keyFile)
		if sslErr != nil {
			log.Fatalf("Failed loading certificates: %v", sslErr)
			return
		}
		opts = append(opts, grpc.Creds(creds))
	}
	s := grpc.NewServer(opts...)

	//custom server
	srv := &handler.Server{}

	rpc.RegisterAlarmsServiceServer(s, srv)

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}


}
