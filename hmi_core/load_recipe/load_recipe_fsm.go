package load_recipe

import (
	"context"
	"fmt"
	"github.com/looplab/fsm"
	"time"
)


const (
	EVT_BEGIN   	= "begin"
	EVT_CHECK   	= "check"
	EVT_EXECUTE  	= "execute"
	EVT_CONFIRM 	= "confirm"
	EVT_END     	= "end"

	ST_IDLE   		= "idle"
	ST_BEGIN   		= "beginning"
	ST_CHECK   		= "checking"
	ST_EXECUTE  	= "executing"
	ST_CONFIRM 		= "confirming"
	ST_END     		= "ending"
)

type LoadRecipeFsm struct {
	Fsm 			*fsm.FSM
	callbacks   	fsm.Callbacks
	adapters      	[]*Adapter
	equipments    	[]*Equipment
	resultChannel 	chan bool
	result       	bool
}

type StateResult struct {
	Val int
}

type StateParameters struct {
	Val int
}

type Adapter struct {
	Delay 	int
	Timeout int64
}

type Equipment struct {
	Tick int
}

func NewLoadRecipeFsm() *LoadRecipeFsm {
	lr := &LoadRecipeFsm{
		Fsm:           nil,
		callbacks:     fsm.Callbacks{},
		resultChannel: make(chan bool),
	}
	lr.init()
	return lr
}


//
func (l *LoadRecipeFsm) init()  {

	f := fsm.NewFSM(
		ST_IDLE,
		fsm.Events{
			{Name: EVT_BEGIN, 		Src: []string{ST_IDLE}, 						Dst: ST_BEGIN},
			{Name: EVT_CHECK, 		Src: []string{ST_BEGIN}, 						Dst: ST_CHECK },
			{Name: EVT_EXECUTE, 	Src: []string{ST_CHECK}, 						Dst: ST_EXECUTE},
			{Name: EVT_CONFIRM,		Src: []string{ST_CHECK, ST_EXECUTE}, 	    	Dst: ST_CONFIRM},
			{Name: EVT_END, 		Src: []string{ST_CONFIRM, ST_BEGIN}, 	   		Dst: ST_END},
		},

		fsm.Callbacks{
			"enter_" + ST_BEGIN 	:   l.onEnterBeginning,
			"enter_" + ST_CHECK		: 	l.onEnterChecking,
			"enter_" + ST_EXECUTE 	:   l.onEnterExecuting,
			"enter_" + ST_CONFIRM	:   l.onEnterConfirm,
			"enter_" + ST_END		:   l.onEnterEnding,
		},
	)

	l.Fsm = f
}

//
func (l *LoadRecipeFsm) _fakeGetAdapterAvailability(v int) int {
	if v == 500 {
		return 1
	} else {
		return 1
	}
}
//
func (l *LoadRecipeFsm) _fakeConfigManagerLoadRecipes(v int) int {
	l.adapters   =  []*Adapter{&Adapter{Delay: 500, Timeout: 3000}, &Adapter{Delay: 1000, Timeout: 3000}}
	l.equipments =  []*Equipment{&Equipment{Tick: 500}, &Equipment{Tick: 1000}, &Equipment{Tick: 2000}}
	return 1
}
//
func (l *LoadRecipeFsm) _fakeEquipmentRecipesAttributes(v int) int {
	if v == 500 {
		return 1
	} else {
		return 1
	}
}
//
func (l *LoadRecipeFsm) _fakeAdapterSetLoadingResult(v int) int {
	if v == 500 {
		return 1
	} else {
		return 1
	}
}
//
func (l *LoadRecipeFsm) _fakeConfigSetLoadRecipesResult(v int) int {
	return 1
}

//
func (l *LoadRecipeFsm) onEnterBeginning(e *fsm.Event) {
	fmt.Println("onEnterBeginning")

	rv := l._fakeConfigManagerLoadRecipes(0)

	l.setResult(ST_BEGIN, &StateResult{Val: rv})
}

//
func (l *LoadRecipeFsm) onEnterChecking(e *fsm.Event) {
	fmt.Println("onEnterChecking")

	responseCh := make(chan int)
	for _, a := range  l.adapters {
		go l.getAdaptersAvailability(a.Delay, a.Timeout, responseCh)
	}

	var result = true
	// wait adapters responseCh
	for i := 0; i < len(l.adapters); i++ {
		v := <-responseCh
		if v == 1 {
			result = result && true
		} else {
			result = result && false
		}
		fmt.Println("RESPONSE=", v)
	}

	if result == true {
		l.setResult(ST_CHECK, &StateResult{Val: 1})
	} else {
		l.setResult(ST_CHECK, &StateResult{Val: 0})
	}
}

//
func (l *LoadRecipeFsm) onEnterExecuting(e *fsm.Event) {
	fmt.Println("onEnterExecuting")

	responseCh := make(chan int)
	for _, e := range  l.equipments {
		go l.setEquipmentRecipesAttributes(e.Tick, 500 , responseCh)
	}

	var result = true
	// wait equipments responseCh
	for i := 0; i < len(l.equipments); i++ {
		v := <-responseCh
		if v == 1 {
			result = result && true
		} else {
			result = result && false
		}
		fmt.Println("Executing response =", v)
	}

	if result == true {
		l.setResult(ST_EXECUTE, &StateResult{Val: 1})
	} else {
		l.setResult(ST_EXECUTE, &StateResult{Val: 0})
	}
}

//
func (l *LoadRecipeFsm) onEnterConfirm(e *fsm.Event) {

	params := l.getParameters(ST_CONFIRM)
	fmt.Println("onEnterConfirm : parameter [", params.Val, "]")

	responseCh := make(chan int)
	for _, a := range  l.adapters {
		go l.setAdaptersLoadingResult(a.Delay, a.Timeout, responseCh)
	}

	var result = true
	// wait adapters responseCh
	for i := 0; i < len(l.adapters); i++ {
		v := <-responseCh
		if v == 1 {
			result = result && true
		} else {
			result = result && false
		}
		fmt.Println("RESPONSE=", v)
	}

	if result == true {
		l.setResult(ST_CONFIRM, &StateResult{Val: 1})
	} else {
		l.setResult(ST_CONFIRM, &StateResult{Val: 0})
	}
}

//
func (l *LoadRecipeFsm) onEnterEnding(e *fsm.Event) {
	params := l.getParameters(ST_END)
	fmt.Println("onEnterEnding : parameter [", params.Val, "]")

	l._fakeConfigSetLoadRecipesResult(0)

	// send response
	l.setResult(ST_END, &StateResult{Val: 1})
}

//
func (l *LoadRecipeFsm) getAdaptersAvailability(delay int, tOut int64, ch chan int){

	timeout, cancel := context.WithTimeout(context.Background(), time.Duration(tOut) * time.Millisecond)
	defer cancel()

	for {
		select {
		case <-timeout.Done():
			ch <- 0
			return
		default:
			val := l._fakeGetAdapterAvailability(delay)
			if val == 1 {
				ch <- 1
				return
			}
		}
		time.Sleep(time.Duration(delay) * time.Millisecond)
	}
}

//
func (l *LoadRecipeFsm) setEquipmentRecipesAttributes(delay int, tOut int64, ch chan int){

	timeout, cancel := context.WithTimeout(context.Background(), time.Duration(tOut) * time.Millisecond)
	defer cancel()

	for {
		select {
		case <-timeout.Done():
			ch <- 0
			return
		default:
			// TODO Implement right routine
			val := l._fakeEquipmentRecipesAttributes(delay)
			if val == 1 {
				ch <- 1
				return
			}
		}
		time.Sleep(time.Duration(delay) * time.Millisecond)
	}
}

//
func (l *LoadRecipeFsm) setAdaptersLoadingResult(delay int, tOut int64, ch chan int){

	timeout, cancel := context.WithTimeout(context.Background(), time.Duration(tOut) * time.Millisecond)
	defer cancel()

	for {
		select {
		case <-timeout.Done():
			ch <- 0
			return
		default:
			val := l._fakeAdapterSetLoadingResult(delay)
			if val == 1 {
				ch <- 1
				return
			}
		}
		time.Sleep(time.Duration(delay) * time.Millisecond)
	}
}

//
func (l *LoadRecipeFsm) setResult(key string, result *StateResult)  {
	l.Fsm.SetMetadata(key+"_result", result)
}

//
func (l *LoadRecipeFsm) getResult(key string) *StateResult {
	data, ok := l.Fsm.Metadata(key+"_result"); if ! ok {
		panic("Invalid status result")
	}
	return data.(*StateResult)
}

//
func (l *LoadRecipeFsm) setParameters(key string, params *StateParameters)  {
	l.Fsm.SetMetadata(key+"_parameters", params)
}

//
func (l *LoadRecipeFsm) getParameters(key string) *StateParameters {
	data, ok := l.Fsm.Metadata(key+"_parameters"); if ! ok {
		panic("Invalid status parameters")
	}
	return data.(*StateParameters)
}

//
func (l *LoadRecipeFsm) Begin() bool {

	err := l.Fsm.Event(EVT_BEGIN)
	if err != nil {
		panic(err.Error())
	}

	res :=  l.getResult(ST_BEGIN)
	var rv = false
	if res.Val == 1 { rv = true  }
	return rv
}

//
func (l *LoadRecipeFsm) Check() bool {
	err := l.Fsm.Event(EVT_CHECK)
	if err != nil {
		panic(err.Error())
	}

	res :=  l.getResult(ST_CHECK)
	var rv = false
	if res.Val == 1 { rv = true  }
	return rv
}

//
func (l *LoadRecipeFsm) Execute() bool {
	err := l.Fsm.Event(EVT_EXECUTE)
	if err != nil {
		panic(err.Error())
	}

	res :=  l.getResult(ST_EXECUTE)
	var rv = false
	if res.Val == 1 { rv = true  }
	return rv
}

//
func (l *LoadRecipeFsm) Confirm(param int) bool {

	l.setParameters(ST_CONFIRM, &StateParameters{Val: param})

	err := l.Fsm.Event(EVT_CONFIRM)
	if err != nil {
		panic(err.Error())
	}

	res :=  l.getResult(ST_CONFIRM)
	var rv = false
	if res.Val == 1 { rv = true  }
	return rv
}

//
func (l *LoadRecipeFsm) End(param int) bool {

	l.setParameters(ST_END, &StateParameters{Val: param})

	err := l.Fsm.Event(EVT_END)
	if err != nil {
		panic(err.Error())
	}

	res :=  l.getResult(ST_END)
	var rv = false
	if res.Val == 1 { rv = true  }
	return rv
}