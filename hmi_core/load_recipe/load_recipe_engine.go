package load_recipe

const (
	OK = 1
	KO = 0
)

type LoadRecipeEngine struct {
	lrf *LoadRecipeFsm
}


func NewEngine() *LoadRecipeEngine {
	return &LoadRecipeEngine{
		lrf: NewLoadRecipeFsm(),
	}
}

func (e *LoadRecipeEngine) Start() bool {

	var rv = false

	rv = e.lrf.Begin()

	if rv == false {
		e.lrf.End(KO)
		return false
	}

	rv = e.lrf.Check()

	if rv == false {

		e.lrf.Confirm(KO)

		e.lrf.End(KO)

		return false
	}

	rv = e.lrf.Execute()

	if rv == false {
		e.lrf.Confirm(KO)

		e.lrf.End(KO)

		return false
	}

	rv = e.lrf.Confirm(OK)

	if rv == false {
		rv = e.lrf.End(KO)
	}

	rv = e.lrf.End(OK)

	return rv
}
