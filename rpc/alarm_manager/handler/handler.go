package handler

import (
	"context"
	"fmt"
	"ftsystem.com/test-alarm-manager/cmd/alarm_manager/commons"
	"ftsystem.com/test-alarm-manager/rpc"
	"google.golang.org/grpc/peer"
	"time"
)

/// server
type Server struct{}

///
func (s *Server) buildAlarmResponse(info string) *rpc.GetAlarmResponse {
	alarms := []*rpc.AlarmData{
		{
			Key:    "key1" + info,
			Status: 0,
		},
	}
	return &rpc.GetAlarmResponse{AlarmData: alarms}
}

///
func (s *Server) sendStream(data string, stream rpc.AlarmsService_GetAlarmsStreamServer) error {

	response := s.buildAlarmResponse(data)
	err := stream.Send(response)
	if err != nil{
		fmt.Printf("Server Err %v\n", err.Error())
		return err
	}
	return nil
}

//
func (s *Server) startSender(l *commons.Listener)  {
	fmt.Println("Single Sender started!")

	go func() {
		for {
			// 1 - Dump alarmlist
			// 2 - Send alarmlist
			data := "$" + l.Data
			err := s.sendStream(data, l.Stream)

			p, _ := peer.FromContext(l.Stream.Context())
			fmt.Println("Sever send data", data, "to", p.Addr)

			if err != nil {
				l.OutChannel <- 1
				fmt.Println("Err from", p, " - Single Sender", l, "STOPPED")
				return
			}
			// 3 - Clear alarmlist??
			time.Sleep(time.Duration(l.Tick) * time.Millisecond)
		}
	}()

}

///
func (s *Server) GetAlarmsStream(request *rpc.GetAlarmStreamRequest, stream rpc.AlarmsService_GetAlarmsStreamServer) error{
	fmt.Printf("GetAlarmsStream function was invoked with %v\n", request)

	listener := commons.NewListener(stream, request.GetParam1(), 500)

	p, _ := peer.FromContext(listener.Stream.Context())
	fmt.Println("Listener instance:", listener, "peer", p.Addr.String())

	s.startSender(listener)

	for {
		select {
		case <- listener.OutChannel:
			break
		}
		fmt.Println("Stop transmission to peer", p)
		return nil
	}
	return nil
}

//
func (s *Server) GetAlarms(ctx context.Context, request *rpc.GetAlarmRequest) (*rpc.GetAlarmResponse, error) {
	panic("implement me")
}