package server_

import (
	"context"
	"fmt"
	"ftsystem.com/test-alarm-manager/cmd/alarm_manager/commons"
	"ftsystem.com/test-alarm-manager/rpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/peer"
	"log"
	"net"
	"time"
	//"google.golang.org/grpc/codes"
	//"google.golang.org/grpc/status"
	"google.golang.org/grpc/credentials"
)



type server struct{
	listeners commons.Listeners
}

func (s *server) AddListener(l *commons.Listener) {
	s.listeners.Add(l)
}

func (s *server) RemoveListenerAt(idx int) {
	s.listeners.RemoveAt(idx)
}

func (s *server) DumpListeners() []*commons.Listener {
	return s.listeners.DumpList()
}

///
func (s *server) buildAlarmResponse(info string) *rpc.GetAlarmResponse {
	alarms := []*rpc.AlarmData{
		{
			Key:    "key1" + info,
			Status: 0,
		},
	}
	return &rpc.GetAlarmResponse{AlarmData: alarms}
}

///
func (s *server) sendStream(data string, stream rpc.AlarmsService_GetAlarmsStreamServer) error {

	response := s.buildAlarmResponse(data)
	err := stream.Send(response)
	if err != nil{
		fmt.Printf("Server Err %v\n", err.Error())
		return err
	}
	return nil
}

//
func (s *server) startSender(l* commons.Listener)  {
	fmt.Println("Single Sender started!")

	go func() {
		for {
			// 1 - Dump alarmlist
			// 2 - Send alarmlist
			data := "$" + l.Data
			err := s.sendStream(data, l.Stream)

			p, _ := peer.FromContext(l.Stream.Context())
			fmt.Println("Sever send data", data, "to", p.Addr)

			if err != nil {
				l.OutChannel <- 1
				fmt.Println("Err from", p, " - Single Sender", l, "STOPPED")
				return
			}
			// 3 - Clear alarmlist??
			time.Sleep(time.Duration(l.Tick) * time.Millisecond)
		}
	}()

}

///
func (s *server) GetAlarmsStream(request *rpc.GetAlarmStreamRequest, stream rpc.AlarmsService_GetAlarmsStreamServer) error{
	fmt.Printf("GetAlarmsStream function was invoked with %v\n", request)

	listener := commons.NewListener(stream, request.GetParam1(), 500)
	s.AddListener(listener)

	p, _ := peer.FromContext(listener.Stream.Context())
	fmt.Println("Listener instance:", listener, "peer", p.Addr.String())

	s.startSender(listener)

	for {
		select {
			case <- listener.OutChannel:
				break
		}
		fmt.Println("Stop transmission to peer", p)
		return nil
	}
	return nil
}
//
func (s *server) GetAlarms(ctx context.Context, request *rpc.GetAlarmRequest) (*rpc.GetAlarmResponse, error) {
	panic("implement me")
}

func main() {
	fmt.Println("Hello world")

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	opts := []grpc.ServerOption{}
	//tls := true
	tls := false
	if tls {
		certFile := "../ssl/server.crt"
		keyFile := "../ssl/server.pem"
		creds, sslErr := credentials.NewServerTLSFromFile(certFile, keyFile)
		if sslErr != nil {
			log.Fatalf("Failed loading certificates: %v", sslErr)
			return
		}
		opts = append(opts, grpc.Creds(creds))
	}

	s := grpc.NewServer(opts...)

	//custom server
	srv := &server{
		listeners: commons.Listeners{},
	}
	rpc.RegisterAlarmsServiceServer(s, srv)

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}


}
